import pgzrun
import random

screen_height = 600
screen_width = 800
grass_size = 70
actor_dim = (96, 128)
ground_level = screen_height - grass_size - actor_dim[1]/2
player = Actor('p3_stand', (actor_dim[0]/2, ground_level))

clouds = [Actor('cloud1', (200, 200)),
          Actor('cloud2', (400, 300)),
          Actor('cloud3', (600, 200)),
          Actor('cloud1', (800, 300))]


ground = []
for i in range((screen_width//grass_size)+2):
    ground.append(Actor('grass', (i * grass_size, screen_height - grass_size/2)))



score = 0
cloud_score = 0
super_position = 0
foton = 0 
jump = 0
cloud_pace = 2
ground_pace = 2
is_rocket = False

jump_limit = 4

lock_jump = False
lock_cloud = False

def draw():
    screen.clear()
    screen.blit('bg_forest', (0, 0))
    #screen.fill('#cff4f7')
    player.draw()
    make_clouds()
    screen.draw.text(
        score_format(score),
        midright=(screen.width - 50, 50),
        color = "darkblue",
        fontsize = 45
    )
    screen.draw.text(
        score_format(cloud_score),
        midleft = (0 + 50, 50),
        color = "darkblue",
        fontsize = 45
    )
    screen.draw.text(
        score_format(jump_limit),
        midleft = (screen.width/2 + 50, 50),
        color = "darkblue",
        fontsize = 45
    )

    make_ground(screen)
    if super_position == 0:
        init_text(screen)

def update():
    global super_position
    global lock_jump
    global jump
    global jump_limit
    if keyboard.SPACE:
        if super_position == 0:
            lock_jump = True
            clock.schedule_unique(unlock_jump, 0.3)
        super_position = 1
        if not lock_jump and jump_limit > 0:
            jump_limit -= 1
            jump = -18 #* (random.random() * 2)
            lock_jump = True
            sounds.p3_jump.play()
    awalk()
    ajump(250, ground_level)
    cloud_move()
    ground_move()

def init_text(screen):
    screen.draw.text(
        "Cloud Collector\n\nPress SPACE",
        center = (screen.width/2, screen.height/2),
        color = "darkgreen",
        fontsize = 70
    )

def score_format(score):
    res = "0" * (5-len(str(score)))
    res += str(score)
    return res

def unlock_jump():
    global lock_jump
    global lock_cloud
    lock_jump = False
    lock_cloud = False

def make_ground(screen, shift = 0):
    for g in ground:
        g.draw()


def awalk():
    global foton
    frame = 4
    if super_position == 1:
        player.image = 'p3_walk%s' % str(round(foton/frame))
    foton += 1
    foton %= 8*frame - frame

def ajump(top = 250, bottom = 484):
    global jump
    global frame
    if jump != 0:
        frame = 0
        player.image = 'p3_up'
        player.y += jump
    if player.y >= bottom:
        unlock_jump()
        jump = 0
    if player.y <= top:
        jump *= (-1)


def cloud_move():
    global cloud_pace
    global super_position
    global cloud_score
    global lock_cloud
    global is_rocket
    if super_position == 1:
        for cloud in clouds:
            if(cloud.distance_to((player.x, player.y)) < 75 and bool(round(random.random()-.4))):
                cloud.image = 'snow_ball1'
                if not lock_cloud:
                    cloud_score += 1
                    lock_cloud = True
            cloud.x -= cloud_pace
            if cloud.x + 64 < 0:
                cloud.x = screen.width + 32 
                tmp = cloud.y * random.random()
                if tmp < 20:
                    cloud.y = tmp + 300
                else:
                    cloud.y = tmp
                if cloud.y < 75 and not is_rocket and bool(round(random.random()-.4)):
                    cloud.image = 'rocket'
                    is_rocket = True
                else:
                    cloud.image = 'cloud%s' % str(1 + round(random.random()) * 2)


def ground_move():
    global ground_pace
    global super_position
    if super_position == 1:
        for g in ground:
            g.x -= ground_pace
            if g.x + 64 < 0:
                g.x = screen.width + 32


def make_clouds():
    for cloud in clouds:
        cloud.draw()

def count_score():
    global score
    global super_position
    if super_position == 0:
        score = 0
    elif super_position == 1:
        score += 1

def reset_rocket():
    global is_rocket
    is_rocket = False

def reset_jump_limit():
    global jump_limit
    jump_limit += 1

clock.schedule_interval(count_score, 0.1)
clock.schedule_interval(reset_rocket, 10)
clock.schedule_interval(reset_jump_limit, 3)
pgzrun.go()
